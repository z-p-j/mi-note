# MiNote

<img src="entry/src/main/resources/base/media/icon.png" width="128px" />

## 项目介绍

&emsp;&emsp;基于OpenHarmony平台的小米笔记第三方客户端。由于开发者之前换了非小米的手机，导致小米笔记中保存的重要内容查看不方便，于是在两年前开发并在github上开源了一款Android平台的第三方小米笔记应用——[MiNote](https://github.com/Z-P-J/MiNote) ，而本仓库是基于ArkUI框架实现的OpenHarmony版本。<br />
&emsp;&emsp;本项目的原理是从小米云服务官网登录小米账户，获取cookie并保存到本地。然后通过cookie去访问小米云服务笔记相关的api接口获取数据，并通过ArkUI原生控件来展示。目前MiNote已实现了登录并获取笔记列表、查看笔记文本内容、搜索笔记、二次校验等功能。

## 开发环境

- DevEco Studio 3.1 Beta1
- SDK API9 3.2.10.6 Beta5

## 截图预览

<img src="screenshot/7.jpg" width="300px" />
<img src="screenshot/0.jpg" width="300px" />
<img src="screenshot/1.jpg" width="300px" />
<img src="screenshot/2.jpg" width="300px" />
<img src="screenshot/3.jpg" width="300px" />
<img src="screenshot/4.jpg" width="300px" />
<img src="screenshot/5.jpg" width="300px" />
<img src="screenshot/6.jpg" width="300px" />





## TODO
- 完善笔记列表的排序
- 实现瀑布流展示笔记数据
- 完善设置页面的功能
- 支持显示笔记内容中的图片、语音等
- 新增笔记分类、回收站等功能
- 新增笔记本地缓存与云端同步机制