/* @file
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


const AUDIO_HTML = "<div class=\"pm-block bloc pm-voice-container\" contenteditable=\"false\">\n" +
    "                    <p class=\"\" data-indentation=\"1\"><br\n" +
    "                            class=\"ProseMirror-trailingBreak\"></p>\n" +
    "                    <div contenteditable=\"false\" class=\"pm-voice\" custom-voice=\"true\" tabindex=\"1\" aria-label=\"这是一条语音信息\">\n" +
    "                        <div class=\"ap-container big\">\n" +
    "                            <div class=\"ap-btn-play-pause\" role=\"button\">\n" +
    "                                <div class=\"ap-icon-play\">\n" +
    "                                    <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 24 24\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect width=\"24\" height=\"24\" rx=\"12\" fill=\"hsla(0,0%,100%,0.6)\"></rect>\n" +
    "                                        <path d=\"M15.7557 10.6046C16.4508 11.0725 16.7984 11.3065 16.9201 11.5998C17.0266 11.8562 17.0266 12.1438 16.9201 12.4003C16.7984 12.6935 16.4508 12.9275 15.7557 13.3954L11.6772 16.1411C10.8091 16.7255 10.375 17.0177 10.0144 16.9992C9.70031 16.983 9.40933 16.8313 9.21878 16.5845C9 16.301 9 15.7826 9 14.7456L9 9.25437C9 8.21742 9 7.69895 9.21878 7.41552C9.40933 7.16865 9.70031 7.01699 10.0144 7.00083C10.375 6.98228 10.8091 7.2745 11.6772 7.85894L15.7557 10.6046Z\"\n" +
    "                                              fill=\"#6200EE\"></path>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"ap-icon-pause\">\n" +
    "                                    <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 68 68\" height=\"1em\" width=\"1em\">\n" +
    "                                        <rect width=\"68\" height=\"68\" rx=\"34\" fill=\"hsla(0,0%,100%,0.6)\"></rect>\n" +
    "                                        <rect x=\"22\" y=\"22\" width=\"8\" height=\"24\" rx=\"2\"\n" +
    "                                              fill=\"#6200EE\"></rect>\n" +
    "                                        <rect x=\"38\" y=\"22\" width=\"8\" height=\"24\" rx=\"2\"\n" +
    "                                              fill=\"#6200EE\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"ap-duration\">00:00</div>\n" +
    "                            <div class=\"tl-container big\">\n" +
    "                                <svg width=\"64\" height=\"16\" viewBox=\"0 0 64 16\" fill=\"none\"\n" +
    "                                     xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                    <rect y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.742808\" fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"17.8271\" y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"8.91357\" y=\"0.74054\" width=\"1.48562\" height=\"14.5185\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"4.45654\" y=\"3.16031\" width=\"1.48561\" height=\"9.67901\" rx=\"0.742807\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"13.3706\" y=\"3.16031\" width=\"1.48562\" height=\"9.67901\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"22.2842\" y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"40.1113\" y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"31.1978\" y=\"0.740509\" width=\"1.48562\" height=\"14.5185\" rx=\"0.74281\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"26.7412\" y=\"3.16031\" width=\"1.48562\" height=\"9.67901\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"35.6548\" y=\"3.16031\" width=\"1.48562\" height=\"9.67901\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"44.5684\" y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.742808\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"62.396\" y=\"4.97513\" width=\"1.48562\" height=\"5.44445\" rx=\"0.74281\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"53.4819\" y=\"0.740509\" width=\"1.48561\" height=\"14.5185\" rx=\"0.742807\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"49.0254\" y=\"3.16031\" width=\"1.48562\" height=\"9.67901\" rx=\"0.74281\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                    <rect x=\"57.939\" y=\"3.16031\" width=\"1.48561\" height=\"9.67901\" rx=\"0.742807\"\n" +
    "                                          fill=\"#FFBB00\"></rect>\n" +
    "                                </svg>\n" +
    "                                <div class=\"tl-bg-cont background big show\">\n" +
    "                                    <svg width=\"142\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"52.9092\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 89.2725 4.33789)\"></rect>\n" +
    "                                        <rect x=\"48.5454\" y=\"4.75342\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 93.6362 4.75342)\"></rect>\n" +
    "                                        <rect x=\"22.3638\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"9.27246\" y=\"4.33838\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 132.909 4.33838)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 119.818 4.33789)\"></rect>\n" +
    "                                        <rect x=\"39.8184\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 102.364 4.33789)\"></rect>\n" +
    "                                        <rect x=\"57.2725\" y=\"3.50684\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 84.9092 3.50684)\"></rect>\n" +
    "                                        <rect x=\"31.0908\" y=\"1.42871\" width=\"1.45455\" height=\"9.14286\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"9.14286\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 111.091 1.42871)\"></rect>\n" +
    "                                        <rect x=\"18\" y=\"4.75391\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"4.90918\" y=\"4.75293\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 137.272 4.75293)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 124.182 4.75342)\"></rect>\n" +
    "                                        <rect x=\"44.1816\" y=\"4.75342\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 98 4.75342)\"></rect>\n" +
    "                                        <rect x=\"61.6362\" y=\"2.26025\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"70.3638\" y=\"2.26025\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 80.5454 2.26025)\"></rect>\n" +
    "                                        <rect x=\"26.7271\" y=\"3.09131\" width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 115.455 3.09131)\"></rect>\n" +
    "                                        <rect x=\"35.4546\" y=\"3.09131\" width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 106.727 3.09131)\"></rect>\n" +
    "                                        <rect x=\"13.6362\" y=\"4.75391\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.54541\" y=\"4.75293\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 141.636 4.75293)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 128.545 4.75342)\"></rect>\n" +
    "                                        <rect x=\"66\" y=\"0.182129\" width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 76.1816 0.182129)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"tl-bg-cont big show\">\n" +
    "                                    <svg width=\"142\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"52.9092\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 89.2725 4.33789)\"></rect>\n" +
    "                                        <rect x=\"48.5454\" y=\"4.75342\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 93.6362 4.75342)\"></rect>\n" +
    "                                        <rect x=\"22.3638\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"9.27246\" y=\"4.33838\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 132.909 4.33838)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 119.818 4.33789)\"></rect>\n" +
    "                                        <rect x=\"39.8184\" y=\"4.33789\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 102.364 4.33789)\"></rect>\n" +
    "                                        <rect x=\"57.2725\" y=\"3.50684\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 84.9092 3.50684)\"></rect>\n" +
    "                                        <rect x=\"31.0908\" y=\"1.42871\" width=\"1.45455\" height=\"9.14286\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"9.14286\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 111.091 1.42871)\"></rect>\n" +
    "                                        <rect x=\"18\" y=\"4.75391\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"4.90918\" y=\"4.75293\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 137.272 4.75293)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 124.182 4.75342)\"></rect>\n" +
    "                                        <rect x=\"44.1816\" y=\"4.75342\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 98 4.75342)\"></rect>\n" +
    "                                        <rect x=\"61.6362\" y=\"2.26025\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"70.3638\" y=\"2.26025\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 80.5454 2.26025)\"></rect>\n" +
    "                                        <rect x=\"26.7271\" y=\"3.09131\" width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 115.455 3.09131)\"></rect>\n" +
    "                                        <rect x=\"35.4546\" y=\"3.09131\" width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"5.81818\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 106.727 3.09131)\"></rect>\n" +
    "                                        <rect x=\"13.6362\" y=\"4.75391\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.54541\" y=\"4.75293\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 141.636 4.75293)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 128.545 4.75342)\"></rect>\n" +
    "                                        <rect x=\"66\" y=\"0.182129\" width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 76.1816 0.182129)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"tl-bg-cont background mid\">\n" +
    "                                    <svg width=\"88\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"26.9092\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"9.45459\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"18.1821\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 63.2729 4.24707)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 80.7275 4.24707)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 72.0005 4.24707)\"></rect>\n" +
    "                                        <rect x=\"22.5459\" y=\"4.66211\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 67.6367 4.66211)\"></rect>\n" +
    "                                        <rect x=\"31.2729\" y=\"3.41602\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"13.8184\" y=\"3.41602\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 58.9092 3.41602)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 76.3638 3.41602)\"></rect>\n" +
    "                                        <rect x=\"5.09131\" y=\"4.66162\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.727539\" y=\"4.66162\" width=\"1.45455\" height=\"2.49351\"\n" +
    "                                              rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 85.0913 4.66162)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 89.4546 4.66162)\"></rect>\n" +
    "                                        <rect x=\"35.6367\" y=\"2.16895\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"44.3638\" y=\"2.16895\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 54.5459 2.16895)\"></rect>\n" +
    "                                        <rect x=\"40.0005\" y=\"0.0908203\" width=\"1.45455\" height=\"11.6364\"\n" +
    "                                              rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 50.1821 0.0908203)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"tl-bg-cont mid\">\n" +
    "                                    <svg width=\"88\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"26.9092\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"9.45459\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"18.1821\" y=\"4.24707\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 63.2729 4.24707)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 80.7275 4.24707)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 72.0005 4.24707)\"></rect>\n" +
    "                                        <rect x=\"22.5459\" y=\"4.66211\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 67.6367 4.66211)\"></rect>\n" +
    "                                        <rect x=\"31.2729\" y=\"3.41602\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"13.8184\" y=\"3.41602\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 58.9092 3.41602)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 76.3638 3.41602)\"></rect>\n" +
    "                                        <rect x=\"5.09131\" y=\"4.66162\" width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.727539\" y=\"4.66162\" width=\"1.45455\" height=\"2.49351\"\n" +
    "                                              rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 85.0913 4.66162)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"2.49351\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 89.4546 4.66162)\"></rect>\n" +
    "                                        <rect x=\"35.6367\" y=\"2.16895\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"44.3638\" y=\"2.16895\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 54.5459 2.16895)\"></rect>\n" +
    "                                        <rect x=\"40.0005\" y=\"0.0908203\" width=\"1.45455\" height=\"11.6364\"\n" +
    "                                              rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 50.1821 0.0908203)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"tl-bg-cont background small\">\n" +
    "                                    <svg width=\"46\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"5.27295\" y=\"4.15625\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.90918\" y=\"4.15625\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 41.6362 4.15625)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 46 4.15625)\"></rect>\n" +
    "                                        <rect x=\"9.63623\" y=\"3.3252\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 37.2729 3.3252)\"></rect>\n" +
    "                                        <rect x=\"14\" y=\"2.07812\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"22.7271\" y=\"2.07812\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 32.9092 2.07812)\"></rect>\n" +
    "                                        <rect x=\"18.3638\" width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 28.5454 0)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <div class=\"tl-bg-cont small\">\n" +
    "                                    <svg width=\"46\" height=\"12\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                        <rect x=\"5.27295\" y=\"4.15625\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"0.90918\" y=\"4.15625\" width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 41.6362 4.15625)\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"3.32468\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 46 4.15625)\"></rect>\n" +
    "                                        <rect x=\"9.63623\" y=\"3.3252\" width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"4.98701\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 37.2729 3.3252)\"></rect>\n" +
    "                                        <rect x=\"14\" y=\"2.07812\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect x=\"22.7271\" y=\"2.07812\" width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"7.48052\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 32.9092 2.07812)\"></rect>\n" +
    "                                        <rect x=\"18.3638\" width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"></rect>\n" +
    "                                        <rect width=\"1.45455\" height=\"11.6364\" rx=\"0.727273\"\n" +
    "                                              transform=\"matrix(-1 0 0 1 28.5454 0)\"></rect>\n" +
    "                                    </svg>\n" +
    "                                </div>\n" +
    "                                <span class=\"tl-thumb tl-thumb-2\" style=\"display: none;\">\n" +
    "                                <svg width=\"9\" height=\"36\" viewBox=\"0 0 9 36\" fill=\"none\"\n" +
    "                                     xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                    <rect x=\"4\" width=\"1\" height=\"32.6667\" rx=\"0.5\" fill=\"white\"></rect>\n" +
    "                                    <path d=\"M4.5 31L7.36441 34.1827C7.99507 34.8834 7.49778 36 6.55505 36H2.44495C1.50222 36 1.00493 34.8834 1.63559 34.1827L4.5 31Z\"\n" +
    "                                          fill=\"white\"></path></svg>\n" +
    "                            </span>\n" +
    "                            </div>\n" +
    "                            <div class=\"ap-btn-delete\" role=\"button\">\n" +
    "                                <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 17 16\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
    "                                    <path fill-rule=\"evenodd\" clip-rule=\"evenodd\"\n" +
    "                                          d=\"M6.7963 0C5.92696 0 5.22222 0.716345 5.22222 1.6V2.88H3.96296H2.38895H1.28704C0.852369 2.88 0.5 3.23817 0.5 3.68C0.5 4.12183 0.852369 4.48 1.28704 4.48H2.38889V11.904C2.38889 13.3377 2.38889 14.0546 2.66339 14.6022C2.90485 15.0839 3.29013 15.4755 3.76402 15.721C4.30276 16 5.00802 16 6.41852 16H11.5815C12.992 16 13.6972 16 14.236 15.721C14.7099 15.4755 15.0951 15.0839 15.3366 14.6022C15.6111 14.0546 15.6111 13.3377 15.6111 11.904V4.48H16.713C17.1476 4.48 17.5 4.12183 17.5 3.68C17.5 3.23817 17.1476 2.88 16.713 2.88H15.611H14.037H12.7778V1.6C12.7778 0.716344 12.073 0 11.2037 0H6.7963ZM11.2037 2.88V1.94531C11.2037 1.59185 10.9218 1.30531 10.5741 1.30531H7.42593C7.07819 1.30531 6.7963 1.59185 6.7963 1.94531V2.88H11.2037ZM3.96296 12.352V4.48H14.037V12.352C14.037 13.0689 14.037 13.4273 13.8998 13.7011C13.7791 13.942 13.5864 14.1378 13.3495 14.2605C13.0801 14.4 12.7275 14.4 12.0222 14.4H5.97778C5.27253 14.4 4.9199 14.4 4.65053 14.2605C4.41359 14.1378 4.22094 13.942 4.10021 13.7011C3.96296 13.4273 3.96296 13.0689 3.96296 12.352Z\"\n" +
    "                                          fill=\"#6200EE\"></path>\n" +
    "                                </svg>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>"


var RICH_EDITOR = {};

//RICH_EDITOR.editor = document.getElementById('editorjs_box');
RICH_EDITOR.noteContent = document.getElementById('note-content');
let noteTitle = document.getElementById('note-title');

if (noteTitle) {
    noteTitle.addEventListener('blur', () => {
        if (noteTitle) {
            // 标题div标签在内容为空时自动添加了一个<br>元素，导致无法触发:empty选择器，所以需要当标题标签内容为空时将器innerHTML置为空
            console.log('textContent=' + noteTitle.textContent + ' innerHTML=' + noteTitle.innerHTML);
            if (noteTitle.textContent == '' && noteTitle.innerHTML != '') {
                noteTitle.innerHTML = ''
            }
        }
    })
    noteTitle.addEventListener('keydown', (e) => {
        console.log('keydown code=' + e.code + " activeId=" + document.activeElement.id)
        if (document.activeElement == noteTitle && e.code === 'Enter') {
            e.preventDefault();
        }
    })
    noteTitle.innerHTML = ''
}

RICH_EDITOR.loadImage = function (id, path) {
    for (let img of document.images) {
        let src = img.getAttribute('fileid');
        console.log('MiNote replaceImage src=' + src);
        if (src == id) {
            console.log('MiNote replaceImage');
            img.setAttribute('width', '100%')
            img.setAttribute('src', path);
        }
    }
};

RICH_EDITOR.setTitle = function (title) {
    var noteTitle = document.getElementById('note-title');
    if (noteTitle) {
        noteTitle.innerHTML = title
    }
}

RICH_EDITOR.setHtml = function (contents) {
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
    if (base64regex.test(contents)) {
        RICH_EDITOR.noteContent.innerHTML = decodeURIComponent(escape(atob(contents)));
    } else {
        RICH_EDITOR.noteContent.innerHTML = contents;

        let audioElements = document.querySelectorAll('div.note-audio')
        if (audioElements) {
            for (let audio of audioElements) {
                audio.innerHTML = AUDIO_HTML
            }
        }
    }
};

RICH_EDITOR.getTitle = function () {
    return document.getElementById('note-title').innerHTML;
};

RICH_EDITOR.getHtml = function () {
    return document.getElementById('note-content').innerHTML;
};

RICH_EDITOR.undo = function () {
    document.execCommand('undo', false, null);
};

RICH_EDITOR.redo = function () {
    document.execCommand('redo', false, null);
};

RICH_EDITOR.setBold = function () {
    document.execCommand('bold');
};

RICH_EDITOR.setItalic = function () {
    document.execCommand('italic', false, null);
};

RICH_EDITOR.setSubscript = function () {
    document.execCommand('subscript', false, null);
};

RICH_EDITOR.setSuperscript = function () {
    document.execCommand('superscript', false, null);
};

RICH_EDITOR.setStrikeThrough = function () {
    document.execCommand('strikeThrough', false, null);
};

RICH_EDITOR.setUnderline = function () {
    document.execCommand('underline', false, null);
};

RICH_EDITOR.getListStyle = function () {
    var selection;
    var type;
    if (window.getSelection) {
        selection = getSelection();
    }
    if (!selection) {
        return
    }
    var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
    try {
        var child = range.commonAncestorContainer;
        for (var i = 0; i < 10; i++) {
            if (child.nodeName === 'OL') {
                console.info('insertOrderedList');
                document.execCommand('insertOrderedList', false, null);
                return child.style['list-style'];
            }
            if (child.nodeName === 'UL') {
                console.info('insertUnorderedList');
                document.execCommand('insertUnorderedList', false, null);
                return child.style['list-style'];
            }
            if (child.parentNode) {
                child = child.parentNode;
            }
        }
    } catch (err) {
        console.error(err);
    }

};

RICH_EDITOR.setNumbers = function () {
    let listStyle = RICH_EDITOR.getListStyle();
    if (listStyle === 'decimal') {
        return;
    }
    document.execCommand('insertOrderedList', false, null);
    var selection;
    var type;
    if (window.getSelection) {
        selection = getSelection();
    }
    if (!selection) {
        return
    }
    var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
    try {
        var child = range.commonAncestorContainer;
        for (var i = 0; i < 10; i++) {
            if (child.nodeName === 'OL') {
                child.style['list-style'] = 'decimal';
                break;
            }
            if (child.parentNode) {
                child = child.parentNode;
            }
        }
    } catch (err) {
        console.error(err);
    }
};

RICH_EDITOR.setABC = function () {
    let listStyle = RICH_EDITOR.getListStyle();
    if (listStyle === 'lower-alpha') {
        return;
    }
    document.execCommand('insertOrderedList', false, null);
    var selection;
    var type;
    if (window.getSelection) {
        selection = getSelection();
    }
    if (!selection) {
        return
    }
    var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
    try {
        var child = range.commonAncestorContainer;
        for (var i = 0; i < 10; i++) {
            if (child.nodeName === 'OL') {
                child.style['list-style'] = 'lower-alpha';
                break;
            }
            if (child.parentNode) {
                child = child.parentNode;
            }
        }
    } catch (err) {
        console.error(err);
    }
};

RICH_EDITOR.setBullets = function () {
    let listStyle = RICH_EDITOR.getListStyle();
    if (listStyle === 'disc') {
        return;
    }
    document.execCommand('insertUnorderedList', false, null);
    var selection;
    var type;
    if (window.getSelection) {
        selection = getSelection();
    }
    if (!selection) {
        return
    }
    var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
    try {
        var child = range.commonAncestorContainer;
        for (var i = 0; i < 10; i++) {
            if (child.nodeName === 'UL') {
                child.style['list-style'] = 'disc';
                break;
            }
            if (child.parentNode) {
                child = child.parentNode;
            }
        }
    } catch (err) {
        console.error(err);
    }
};

RICH_EDITOR.setSquare = function () {
    let listStyle = RICH_EDITOR.getListStyle();
    if (listStyle === 'square') {
        return;
    }
    document.execCommand('insertUnorderedList', false, null);
    var selection;
    var type;
    if (window.getSelection) {
        selection = getSelection();
    }
    if (!selection) {
        return
    }
    var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
    try {
        var child = range.commonAncestorContainer;
        for (var i = 0; i < 10; i++) {
            if (child.nodeName === 'UL') {
                child.style['list-style'] = 'square';
                break;
            }
            if (child.parentNode) {
                child = child.parentNode;
            }
        }
    } catch (err) {
        console.error(err);
    }
};

RICH_EDITOR.setTextColor = function (color) {
    document.execCommand('foreColor', false, color);
};

RICH_EDITOR.setFontSize = function (fontSize) {
    document.execCommand('fontSize', false, fontSize);
};

RICH_EDITOR.execFontSize = function (size, unit) {
    if (size === '12') {
        document.execCommand('fontSize', false, 3);
    } else if (size === '16') {
        document.execCommand('fontSize', false, 4);
    } else if (size === '20') {
        document.execCommand('fontSize', false, 5);
    } else if (size === '24') {
        document.execCommand('fontSize', false, 6);
    } else if (size === '28') {
        document.execCommand('fontSize', false, 7);
    }
};

var pad = 24;
RICH_EDITOR.setIndent = function () {
    var parents = document.getElementById('editorjs_box');
    parents.removeAttribute('padding-left');
    if (pad >= 408) {
        return
    }
    pad = pad + 24;
    parents.style.paddingLeft = pad + 'px';
};

RICH_EDITOR.setOutdent = function () {
    var parents = document.getElementById('editorjs_box');
    parents.removeAttribute('padding-left');
    if (pad === 24) {
        parents.style.paddingLeft = 24 + 'px';
    } else {
        pad = pad - 24;
        parents.style.paddingLeft = pad + 'px';
    }
};

RICH_EDITOR.setJustifyLeft = function () {
    document.execCommand('justifyLeft', false, null);
};

RICH_EDITOR.setJustifyCenter = function () {
    document.execCommand('justifyCenter', false, null);
};

RICH_EDITOR.setJustifyRight = function () {
    document.execCommand('justifyRight', false, null);
};

RICH_EDITOR.insertImage = function (url) {
    var html = '<br></br><img src="' + url
    + '" alt="picvision" style="margin:0px auto;width:90%;display:table-cell;'
    + 'vertical-align:middle;border-radius:10px;max-width:90%" /><br></br>';
    document.getElementById('editorjs_box').innerHTML += html
    document.getElementById('editorjs_box').scrollIntoView(false);
};

RICH_EDITOR.insertHTML = function (html) {
    document.execCommand('insertHTML', false, html);
};

RICH_EDITOR.setDone = function () {
    var html = '<input type="checkbox" checked="checked"/> &nbsp;';
    document.execCommand('insertHTML', false, html);
};

RICH_EDITOR.addTodo = function (e) {
    var KEY_ENTER;
    KEY_ENTER = 13;
    if (e.which === KEY_ENTER) {
        var node = RICH_EDITOR.getSelectedAnchorNode();
        if (node && node.nodeName === '#text') {
            node = node.parentElement;
        }
        if (node && node.nodeName === 'SPAN' && node.previousElementSibling
        && node.previousElementSibling.className === 'note-checkbox') {
            RICH_EDITOR.setTodo();
            e.preventDefault();
        }
    }
};

RICH_EDITOR.setTodo = function () {
    var parent = document.getElementById('editorjs_box');
    var isContentEmpty = parent.innerHTML.trim().length === 0 || parent.innerHTML === '<br>';
    var html = (isContentEmpty ? '' : '<br/>')
    + '<input name="checkbox" type="checkbox" onclick="onCheckChange(this)" class="note-checkbox">'
    + '<span class="note-checkbox-txt">&nbsp;</span>';
    document.execCommand('insertHTML', false, html);
};

function onCheckChange(checkbox) {
    if (checkbox.checked === true) {
        checkbox.setAttribute('checked', 'checked');
    } else {
        checkbox.removeAttribute('checked');
    }
}

RICH_EDITOR.restorerange = function () {
    var selection = window.getSelection();
    selection.removeAllRanges();
    var range = document.createRange();
    range.setStart(RICH_EDITOR.currentSelection.startContainer, RICH_EDITOR.currentSelection.startOffset);
    range.setEnd(RICH_EDITOR.currentSelection.endContainer, RICH_EDITOR.currentSelection.endOffset);
    selection.addRange(range);
};

// 获取光标开始位置归属节点

RICH_EDITOR.getSelectedAnchorNode = function () {
    var node;
    var selection;
    if (window.getSelection) {
        selection = getSelection();
        node = selection.anchorNode;
    }
    if (!node && document.selection) {
        selection = document.selection;
        var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
        node = range.commonAncestorContainer ? range.commonAncestorContainer : range.parentElement
                                                                                   ? range.parentElement() : range.item(0);
    }
    return node;
};

RICH_EDITOR.cancelSelection = function () {
    var selection = window.getSelection();
    selection.removeAllRanges();
}

var callBackToApp;

function getHtmlContent() {
    console.log('getHtmlContent');
    var htmlString = RICH_EDITOR.getHtml();
    let imgName = getImagePathFromContent(htmlString);
    htmlString = window.btoa(unescape(encodeURIComponent(htmlString)));
    callBackToApp.callbackImagePath(imgName);
    var str = callBackToApp.callbackhtml(htmlString);
    console.log('getHtmlContent end');
}

function saveHtmlContent() {
    console.log('saveHtmlContent');
    var htmlString = RICH_EDITOR.getHtml();
    let imgName = getImagePathFromContent(htmlString);
    htmlString = window.btoa(unescape(encodeURIComponent(htmlString)));

    callBackToApp.callbackImagePath(imgName);
    var str = callBackToApp.callbackhtmlSave(htmlString);
    console.log('saveHtmlContent end');
}

function getImagePathFromContent(contentInfo) {
    let imgReg = /<img[^>]+>/g;
    let imgName = "";
    let srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
    let imgArray = contentInfo.match(imgReg);
    // 取第一张图片做为标题栏后的缩略图
    if (imgArray && imgArray.length > 0) {
        let src = imgArray[0].match(srcReg);
        if (src != null && src.length > 1) {
            imgName = src[1];
            if (imgName.indexOf('shuxue.png') >= 0 || imgName.indexOf('cake.png') >= 0) {
                imgName = "/res/" + imgName;
            }
        }
    }
    return imgName;
}

function scheduledSaveContent() {
    console.info('scheduledSaveContent');
    var htmlString = RICH_EDITOR.getHtml();
    let imgName = getImagePathFromContent(htmlString);
    htmlString = window.btoa(unescape(encodeURIComponent(htmlString)));
    callBackToApp.callbackImagePath(imgName);
    var str = callBackToApp.callbackScheduledSave(htmlString);
    console.info('scheduledSaveContent end');
}

document.body.addEventListener('paste', (event) => {
    let length = event.clipboardData.items.length;
    if (length > 0) {
        let file = event.clipboardData.items[0].getAsFile();
        const reader = new FileReader();
        reader.onloadend = () => {
            callBackToApp.callbackPasteImage(reader.result);
        }
        reader.readAsDataURL(file);
        event.preventDefault();
    }
});

RICH_EDITOR.getFontSizes = function () {
    document.execCommand('fontSize', false, null);
    var fontElements = window.getSelection().anchorNode.parentNode;
    var getSize = parseInt(window.getComputedStyle(fontElements, null).fontSize)
    var str = callBackToApp.callbackGetSize(getSize);
};

RICH_EDITOR.insertImageHtml = function (contents) {
    let selection = window.getSelection();
    if (!selection.rangeCount)
        return false;
    selection.deleteFromDocument();
    let img = document.createElement('img');
    img.src = contents;
    selection.getRangeAt(0).insertNode(img);
};

document.addEventListener('click', (e) => {
    console.info(`lsq: e is ${JSON.stringify(e)}`)
    var parent = document.getElementById('editorjs_box');
    if (parent.id !== 'editorjs_box') {
        e.preventDefault()
    }
})

//document.getElementById('addToDo').addEventListener('click', () => {
//  callBackToApp.addToDo()
//})
//
//document.getElementById('chooseStyle').addEventListener('click', () => {
//  callBackToApp.chooseStyle()
//})
//
//document.getElementById('openAlbum').addEventListener('click', () => {
//  callBackToApp.openAlbum()
//})

function changeSizeToRk() {
    document.getElementById('img1').style.width = '40px';
    document.getElementById('img1').style.height = '40px';
    document.getElementById('img2').style.width = '40px';
    document.getElementById('img2').style.height = '40px';
    document.getElementById('img3').style.width = '40px';
    document.getElementById('img3').style.height = '40px';
    document.getElementById('lable1').style.fontSize = '20px';
    document.getElementById('lable2').style.fontSize = '20px';
    document.getElementById('lable3').style.fontSize = '20px';
}

function changeSizeToPhone() {
    document.getElementById('img1').style.width = '24px';
    document.getElementById('img1').style.height = '24px';
    document.getElementById('img2').style.width = '24px';
    document.getElementById('img2').style.height = '24px';
    document.getElementById('img3').style.width = '24px';
    document.getElementById('img3').style.height = '24px';
    document.getElementById('lable1').style.fontSize = '12px';
    document.getElementById('lable2').style.fontSize = '12px';
    document.getElementById('lable3').style.fontSize = '12px';
}

function changeSizeToTablet() {
    document.getElementById('img1').style.width = '28px';
    document.getElementById('img1').style.height = '28px';
    document.getElementById('img2').style.width = '28px';
    document.getElementById('img2').style.height = '28px';
    document.getElementById('img3').style.width = '28px';
    document.getElementById('img3').style.height = '28px';
    document.getElementById('lable1').style.fontSize = '12px';
    document.getElementById('lable2').style.fontSize = '12px';
    document.getElementById('lable3').style.fontSize = '12px';
}

function hiddenButton() {
    document.getElementById('buttonBox').style.display = 'none';
}

RICH_EDITOR.getFocus = function () {
    return document.getElementById('editorjs_box').focus();
}

//document.getElementById('editorjs_box').addEventListener('click', () => {
//    //  if (callBackToApp.getBreakPoint() === 'sm') {
//    //    document.getElementById('buttonBox').style.display = 'flex';
//    //  }
//})


//

const audioPlayer = document.getElementById('note-audio-player')

audioPlayer.addEventListener('play', () => {
    let fileid = audioPlayer.fileid
    console.log('play fileid=' + fileid)

    let btnPlay = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-play')
    if (btnPlay) {
        btnPlay.style.display = 'none'
    }

    let btnPause = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-pause')
    if (btnPause) {
        btnPause.style.display = 'initial'
    }

})

audioPlayer.addEventListener('pause', () => {
    let fileid = audioPlayer.fileid
    console.log('pause fileid=' + fileid)

    let btnPause = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-pause')
    if (btnPause) {
        btnPause.style.display = 'none'
    }

    let btnPlay = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-play')
    if (btnPlay) {
        btnPlay.style.display = 'initial'
    }
})

// duration: 秒
function formatDuration(duration) {
    if (!duration) {
        return '00:00'
    }
    let ms = duration * 1000
    if (duration < 1) {
        return '00:' + Math.floor(ms) % 1000
    }
    let time = {
        day: Math.floor(ms / 86400000),
        hour: Math.floor(ms / 3600000) % 24,
        minute: Math.floor(ms / 60000) % 60,
        second: Math.floor(ms / 1000) % 60,
        millisecond: Math.floor(ms) % 1000
    }

    return Object.entries(time)
        .filter(val => val[1] !== 0 && val[1] !== NaN)
        .map(([key, val]) => val < 10 ? `0${val}` : `${val}`)
        .join(':')
}

audioPlayer.addEventListener('timeupdate', () => {
    let fileid = audioPlayer.fileid
    console.log('timeupdate fileid=' + fileid + ' duration=' + audioPlayer.duration + ' currentTime=' + audioPlayer.currentTime)

    let duration = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-duration')
    if (duration) {
        console.log(formatDuration(audioPlayer.duration))
        duration.innerHTML = formatDuration(audioPlayer.currentTime)
    }

})

function initAudioElement(fileid, src) {
    let btnPause = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-pause')
    if (btnPause) {
        btnPause.onclick = () => {
            pauseNoteAudio(fileid)
        }
    }

    let btnPlay = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-play')
    if (btnPlay) {
        btnPlay.onclick = () => {
            playNoteAudio(fileid, src)
        }
    }

    let btnDelete = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-btn-delete')
    if (btnDelete) {
        btnDelete.onclick = () => {
            deleteNoteAudio(fileid)
        }
    }
}

RICH_EDITOR.initAudio = function (fileid, src) {
    initAudioElement(fileid, src)
}

function pauseAudio() {
    audioPlayer.pause()
    let fileid = audioPlayer.fileid
    if (fileid) {
        let btnPause = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-pause')
        if (btnPause) {
            btnPause.style.display = 'none'
        }

        let btnPlay = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-play')
        if (btnPlay) {
            btnPlay.style.display = 'initial'
        }

        let duration = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-duration')
        if (duration) {
            console.log(formatDuration(audioPlayer.duration))
            duration.innerHTML = '00:00'
        }
    }
}

function playAudio() {
    audioPlayer.play()
    let fileid = audioPlayer.fileid
    if (fileid) {
        let btnPlay = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-play')
        if (btnPlay) {
            btnPlay.style.display = 'none'
        }

        let btnPause = document.querySelector('div.note-audio[fileid="' + fileid + '"] div.ap-icon-pause')
        if (btnPause) {
            btnPause.style.display = 'initial'
        }
    }
}

function playNoteAudio(fileid, src) {
    console.log('playNoteAudio fileid=' + fileid + ' src=' + src)

    pauseAudio()
    audioPlayer.setAttribute('fileid', fileid)
    audioPlayer.fileid = fileid
    audioPlayer.src = src
    audioPlayer.play()
}

function pauseNoteAudio(fileid) {
    console.log('pauseNoteAudio fileid=' + fileid)
    if (fileid === audioPlayer.getAttribute('fileid')) {
        pauseAudio()
    }
}

function deleteNoteAudio(fileid) {
    console.log('deleteNoteAudio fileid=' + fileid)
    if (fileid === audioPlayer.getAttribute('fileid')) {
        pauseAudio()
        audioPlayer.setAttribute('fileid', '')
        audioPlayer.fileid = ''
        audioPlayer.src = ''
    }
    let audio = document.querySelector('div.note-audio[fileid="' + fileid + '"]')
    if (audio) {
        audio.remove()
    }
}

//// TODO 测试
//let audio = document.querySelector('div.note-audio[fileid="123456789"]')
//if (audio) {
//    audio.innerHTML = AUDIO_HTML
//    initAudioElement('123456789', 'test1.mp3')
//}
//
//audio = document.querySelector('div.note-audio[fileid="987654321"]')
//if (audio) {
//    audio.innerHTML = AUDIO_HTML
//    initAudioElement('987654321', 'test2.mp3')
//}