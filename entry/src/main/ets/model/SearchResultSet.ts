import { SearchResult } from './NoteItem';


export default class SearchResultSet {

    private results: SearchResult[] = []

    private noteCount: number = -1

    push(result: SearchResult) {
        this.results.push(result)
    }

    get length() {
        return this.results.length
    }

    getNoteCount() {
        if (this.noteCount < 0) {
            let set = new Set()
            this.results.forEach((result) => {
                set.add(result.getId())
            })
            this.noteCount = set.size
        }
        return this.noteCount
    }

    getResults() {
        return this.results
    }

}