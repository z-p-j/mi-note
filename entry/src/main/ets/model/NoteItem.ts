export class NoteItem {
    private id: string
    private folderId: string
    private createDate: number
    private modifyDate: number

    private snippet: string
    private status: string
    private subject: string
    private tag: string
    private type: string
    private colorId: number
    private alertTag: number
    private alertDate: number

    private modifyDateStr

    getId() {
        return this.id;
    }

    getFolderId() {
        return this.folderId;
    }

    getSnippet() {
        return this.snippet;
    }

    getModifyDateStr() {
        if (!this.modifyDateStr) {
            this.modifyDateStr = new Date(this.modifyDate).toDateString()
        }
        return this.modifyDateStr
    }

    static from(entry: Object) {
        let item = new NoteItem()

        item.id = entry['id']
        item.folderId = entry['folderId']
        item.createDate = entry['createDate']
        item.modifyDate = entry['modifyDate']
        item.snippet = entry['snippet']
        item.status = entry['status']
        item.subject = entry['subject']
        item.tag = entry['tag']
        item.type = entry['type']
        item.colorId = entry['colorId']
        item.alertTag = entry['alertTag']
        item.alertDate = entry['alertDate']
        return item
    }
}

export class SearchResult {
    private id: string
    private snippet: string
    private start: number
    private stop: number
    private modifyDate: string

    private beforeWord: string
    private keyword: string
    private afterWord: string

    constructor(id: string, snippet: string, modifyDate: string, start: number, stop: number) {
        this.id = id
        this.snippet = snippet
        this.modifyDate = modifyDate
        this.start = start
        this.stop = stop


        this.beforeWord = snippet.substring(0, start).trimStart()
        this.keyword = snippet.substring(start, stop)
        this.afterWord = snippet.substring(stop).trimEnd()
    }

    getId() {
        return this.id
    }

    getSnippet() {
        return this.snippet
    }

    getModifyDate() {
        return this.modifyDate
    }

    getStart() {
        return this.start
    }

    getStop() {
        return this.stop
    }

    getBeforeWord() {
        return this.beforeWord
    }

    getKeyword() {
        return this.keyword
    }

    getAfterWord() {
        return this.afterWord
    }

}