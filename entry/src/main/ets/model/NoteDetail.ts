import Logger from '../utils/Logger'

/**
 * 笔记附件信息
 */
export declare interface AttachmentInfo {

    digest: string
    mimeType: string
    fileId: string

}

/**
 * 笔记配置
 */
export declare interface NoteSetting {

    themeId?: number
    stickyTime?: number
    version?: number
    attachments?: AttachmentInfo[]

}

/**
 * 笔记详情
 */
export declare interface NoteDetail {

    id: string
    content: string
    createDate?: number
    modifyDate?: number
    colorId?: number
    folderId?: number
    type?: string
    status?: string
    setting?: NoteSetting


}

/**
 * 解析笔记详情
 * @param entry
 */
export function parseNoteDetail(entry) {
    let detail: NoteDetail = {
        id: entry['id'],
        content: entry['content'],
        createDate: entry['createDate'],
        modifyDate: entry['modifyDate']
    }

    let setting = entry['setting']
    if (setting) {
        detail.setting = {
            version: setting['version'],
            themeId: setting['themeId'],
            stickyTime: setting['stickyTime']
        }

        let attachData = setting['data']
        if (attachData) {
            let attachments: AttachmentInfo[] = []
            Logger.e(this, 'parseNoteDetail data=' + JSON.stringify(attachData))
            for (let data of attachData) {
                attachments.push({
                    digest: data['digest'],
                    mimeType: data['mimeType'],
                    fileId: data['fileId']
                })
            }
            detail.setting.attachments = attachments
        }
    }
    return detail
}



