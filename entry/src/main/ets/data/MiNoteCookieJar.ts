
const KEY = 'minote_cookie'

/**
 * cookie保存与获取
 */
export default class MiNoteCookieJar {

    static getCookie(): string {
        if (!AppStorage.Has(KEY)) {
            // 初始化Persist属性
            PersistentStorage.PersistProp(KEY, '')
        }
        return AppStorage.Get(KEY)
    }

    static setCookie(cookie: string) {
        if (AppStorage.Has(KEY)) {
            AppStorage.Set(KEY, cookie)
        } else {
            // 初始化并保存Persist属性的值
            PersistentStorage.PersistProp(KEY, cookie)
        }
    }

}