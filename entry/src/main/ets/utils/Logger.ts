import hilog from '@ohos.hilog'

class Logger {
    private domain: number
    private prefix: string
    private format: string = '%{public}s, %{public}s'

    constructor(prefix: string) {
        this.prefix = prefix
        this.domain = 0xFF00
    }

    d(tag: any, ...args: string[]) {
        hilog.debug(this.domain, this.prefix, this.format, this.wrapArgs(tag, args))
    }

    i(tag: any, ...args: string[]) {
        hilog.info(this.domain, this.prefix, this.format, this.wrapArgs(tag, args))
    }

    w(tag: any, ...args: string[]) {
        hilog.warn(this.domain, this.prefix, this.format, this.wrapArgs(tag, args))
    }

    e(tag: any, ...args: string[]) {
        hilog.error(this.domain, this.prefix, this.format, this.wrapArgs(tag, args))
    }

    wrapArgs(tag: any, args: string[]): string[] {
        let name: string;
        if (tag instanceof Object) {
            name = tag.constructor.name;
        } else if (tag instanceof Function) {
            name = tag.toString();
        } else {
            name = '' + tag;
        }
        if (name) {
            if (args) {
                args.splice(0, 0, name);
            } else {
                args = [name];
            }
        }
        return args;
    }

}

export default new Logger('[MiNote]')